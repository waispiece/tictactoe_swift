//
//  ViewController.swift
//  TicTacToe
//
//  Created by Wais Al Korni on 10/14/14.
//  Copyright (c) 2014 Ice House. All rights reserved.
//

import UIKit

let numberOfSquares = 9
var squares = [Square](count: numberOfSquares, repeatedValue: SquareEmpty())

class ViewController: UIViewController, TicTacToeDelegate {
    let ticTacToe = TicTacToe(initSquares: squares)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ticTacToe.delegate = self
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        choosePlayer()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func choosePlayer() {
        var chooseAlertController: UIAlertController = UIAlertController(title: "Choose Player", message: "Which player that you want to beat?", preferredStyle: .Alert)
        let multiplayerAction = UIAlertAction(title: "Another Player", style: .Default) { (action: UIAlertAction!) -> Void in
            self.ticTacToe.resetGame()
            self.ticTacToe.play(beatAI: false, computerFirstMove: false)
        }
        
        let beatAIAction = UIAlertAction(title: "Beat AI", style: .Default) { (action: UIAlertAction!) -> Void in
            self.showFirstMoveQuestion()
        }
        
        chooseAlertController.addAction(multiplayerAction)
        chooseAlertController.addAction(beatAIAction)
        self.presentViewController(chooseAlertController, animated: true, completion: nil)
    }
    
    func showFirstMoveQuestion() {
        var firstMoveAlertController: UIAlertController = UIAlertController(title: "Who is first move?", message: "", preferredStyle: .Alert)
        let meAction = UIAlertAction(title: "Me", style: .Default) { (action: UIAlertAction!) -> Void in
            self.ticTacToe.resetGame()
            self.ticTacToe.play(beatAI: true, computerFirstMove: false)
        }
        
        let computerAction = UIAlertAction(title: "Computer", style: .Default) { (action: UIAlertAction!) -> Void in
            self.ticTacToe.resetGame()
            self.ticTacToe.play(beatAI: true, computerFirstMove: true)
        }
        
        firstMoveAlertController.addAction(meAction)
        firstMoveAlertController.addAction(computerAction)
        self.presentViewController(firstMoveAlertController, animated: true, completion: nil)
    }
    
    func showPopUpOnEndingGame(#titlePopUp: String, messagePopUp: String)
    {
        var endingGamePopUp: UIAlertController = UIAlertController(title: titlePopUp, message: messagePopUp, preferredStyle: .Alert)
        let tryAgainAction = UIAlertAction(title: "Try Again", style: .Default) { (action: UIAlertAction!) -> Void in
            self.choosePlayer()
        }
        
        endingGamePopUp.addAction(tryAgainAction)
        self.presentViewController(endingGamePopUp, animated: true, completion: nil)
    }
    
    func prepareMessageAndShowIt(game: TicTacToe) {
        var titlePopUp: String = ""
        var messagePopUp: String = ""
        
        switch game.gameWinner {
        case .None:
            titlePopUp = ""
            messagePopUp = ""
        case .PlayerOneWin:
            if game.beatAI && !game.computerFirstMove {
                titlePopUp = "YOU WIN"
                messagePopUp = "Congratulation. You win the game."
            }else if game.beatAI && game.computerFirstMove {
                titlePopUp = "YOU LOSE"
                messagePopUp = "Poor. You lose the game."
            }else {
                titlePopUp = "PLAYER ONE WIN"
                messagePopUp = "Congratulation for Player One."
            }
        case .PlayerTwoWin:
            if game.beatAI && game.computerFirstMove {
                titlePopUp = "YOU WIN"
                messagePopUp = "Congratulation. You win the game."
            }else if game.beatAI && !game.computerFirstMove {
                titlePopUp = "YOU LOSE"
                messagePopUp = "Poor. You lose the game."
            }else {
                titlePopUp = "PLAYER TWO WIN"
                messagePopUp = "Congratulation for Player Two."
            }
        case .Draw:
            titlePopUp = "DRAW"
            messagePopUp = "You all have the same strength and power."
        }
        
        self.showPopUpOnEndingGame(titlePopUp: titlePopUp, messagePopUp: messagePopUp)
    }
    
    //Actions
    @IBAction func squareTapped(sender: UITapGestureRecognizer) {
        if !ticTacToe.isComputerTurn() {
            var tappedImageView: UIImageView = sender.view as UIImageView
            self.ticTacToe.changeSquareStatus(tappedImageView.tag)
        }
    }
    
    //TicTacToe Delegates
    func gameNeedRedrawBoard(game: TicTacToe) {
        for view in self.view.subviews {
            if view is UIImageView {
                var imageView: UIImageView = view as UIImageView
                var imageName: String = game.boardObject.elementAtIndex(imageView.tag).imageName
                imageView.image = UIImage(named: imageName)
            }
        }
    }
    
    func gameDidOver(game: TicTacToe) {
        self.prepareMessageAndShowIt(game)
    }
}