//
//  Board.swift
//  TicTacToe
//
//  Created by Wais Al Korni on 10/28/14.
//  Copyright (c) 2014 Ice House. All rights reserved.
//

import Foundation

func == (left: Square, right: Square) -> Bool {
    return left.imageName == right.imageName
}

func != (left: Square, right: Square) -> Bool {
    return left.imageName != right.imageName
}

class Board
{
    private var board: [Square]
    private var winPositions = [[0, 1, 2], [0, 4, 8], [0, 3, 6], [1, 4, 7], [3, 4, 5], [2, 4, 6], [2, 5, 8], [6, 7, 8]]
    
    init(board: [Square]) {
        self.board = board
    }
    
    func elementAtIndex(index: Int) -> Square {
        return board[index]
    }
    
    func cleanBoard() {
        for var i = 0; i < board.count; i++ {
            board[i] = SquareEmpty()
        }
    }
    
    func replaceElement(#atIndex: Int, withElement: Square) {
        board[atIndex] = withElement
    }
    
    func isDraw() -> Bool {
        for square in board {
            if square == SquareEmpty() {
                return false
            }
        }
        return true
    }
    
    func isWin() -> Bool {
        var win = false
        for winPosition in winPositions {
            if board[winPosition[0]] == SquareEmpty() {
                continue
            }
            
            if board[winPosition[0]] == board[winPosition[1]] && board[winPosition[1]] == board[winPosition[2]] {
                win = true
                break
            }
        }
        
        return win
    }
    
    func winner() -> TicTacToe.GameWinner {
        var square: Square = SquareEmpty()
        
        for winPosition in winPositions {
            if board[winPosition[0]] == SquareEmpty() {
                continue
            }
            
            if board[winPosition[0]] == board[winPosition[1]] && board[winPosition[1]] == board[winPosition[2]] {
                square = board[winPosition[0]]
                break
            }
        }
        
        if square == SquareCross() {
            return TicTacToe.GameWinner.PlayerOneWin
        }else if square == SquareCircle() {
            return TicTacToe.GameWinner.PlayerTwoWin
        }else {
            return TicTacToe.GameWinner.None
        }
    }
    
    func gameScore() -> Int {
        var score: Int = 0
        for indecies in winPositions {
            if board[indecies[0]] == SquareEmpty() {
                continue
            }
            
            var square = board[indecies[0]]
            if board[indecies[0]] == board[indecies[1]] && board[indecies[1]] == board[indecies[2]] {
                score = (square == SquareCross()) ? TicTacToe.GameScore.Win.rawValue : TicTacToe.GameScore.Lose.rawValue
                break
            }
        }
        
        return score
    }
    
    func generateValidMoves() -> Array<Int>
    {
        var list: Array<Int> = [Int]()
        for var i: Int = 0; i < board.count; ++i
        {
            if board[i] == SquareEmpty() {
                list.append(i)
            }
        }
        
        return list
    }
}