//
//  AI.swift
//  TicTacToe
//
//  Created by Wais Al Korni on 10/28/14.
//  Copyright (c) 2014 Ice House. All rights reserved.
//

import Foundation

class AI
{
    private var boardObject: Board
    
    init(board: Board) {
        self.boardObject = board
    }
    
    func miniMax(depth: Int) -> Int
    {
        var bestScore: Int = Int.max
        
        var bestMoves: Array<Int> = [Int]()
        var validMoves: Array<Int> = boardObject.generateValidMoves()
        
        while !validMoves.isEmpty
        {
            boardObject.replaceElement(atIndex: validMoves[0], withElement: SquareCircle())
            var score: Int = maxMove(depth, alpha: TicTacToe.GameScore.Lose.rawValue, beta: TicTacToe.GameScore.Win.rawValue)
            if score < bestScore {
                bestScore = score
                bestMoves.removeAll(keepCapacity: false)
                bestMoves.append(validMoves[0])
            }else if score == bestScore {
                bestMoves.append(validMoves[0])
            }
            
            boardObject.replaceElement(atIndex: validMoves[0], withElement: SquareEmpty())
            validMoves.removeAtIndex(0)
        }
        
        var index = bestMoves.count
        if index > 0 {
            var randomValue: Int = Int(arc4random())
            index = randomValue % index
        }
        
        return bestMoves[index]
    }
    
    func maxMove(depth: Int, alpha: Int, beta: Int) -> Int
    {
        var moveScore: Int = boardObject.gameScore()
        if boardObject.isWin() || boardObject.isDraw() || depth == 0 || alpha >= beta {
            return moveScore
        }
        
        var bestScore = Int.min
        var validMoves: Array<Int> = boardObject.generateValidMoves()
        while !validMoves.isEmpty {
            boardObject.replaceElement(atIndex: validMoves[0], withElement: SquareCross())
            var score = minMove(depth-1, alpha: max(bestScore, alpha), beta: beta)
            bestScore = max(score, bestScore)
            boardObject.replaceElement(atIndex: validMoves[0], withElement: SquareEmpty())
            validMoves.removeAtIndex(0)
        }
        
        return bestScore
    }
    
    func minMove(depth: Int, alpha: Int, beta: Int) -> Int
    {
        var moveScore: Int = boardObject.gameScore()
        if boardObject.isWin() || boardObject.isDraw() || depth == 0 || alpha >= beta {
            return moveScore
        }
        
        var bestScore: Int = Int.max
        var validMoves: Array<Int> = boardObject.generateValidMoves()
        while !validMoves.isEmpty
        {
            boardObject.replaceElement(atIndex: validMoves[0], withElement: SquareCircle())
            var score: Int = maxMove(depth-1, alpha: alpha, beta: min(bestScore, beta))
            bestScore = min(score, bestScore)
            boardObject.replaceElement(atIndex: validMoves[0], withElement: SquareEmpty())
            validMoves.removeAtIndex(0)
        }
        
        return bestScore
    }
}