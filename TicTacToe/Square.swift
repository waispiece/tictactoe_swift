//
//  Square.swift
//  TicTacToe
//
//  Created by Wais Al Korni on 10/15/14.
//  Copyright (c) 2014 Ice House. All rights reserved.
//

import Foundation

class Square {
    var imageName: String
    
    init() {
        imageName = ""
    }
}

class SquareCross: Square {
    override init() {
        super.init()
        imageName = "cross.png"
    }
}

class SquareCircle: Square {
    override init() {
        super.init()
        imageName = "circle.png"
    }
}

class SquareEmpty: Square {
    override init() {
        super.init()
        imageName = "frame.png"
    }
}