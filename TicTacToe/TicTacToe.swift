//
//  TicTacToe.swift
//  TicTacToe
//
//  Created by Wais Al Korni on 10/15/14.
//  Copyright (c) 2014 Ice House. All rights reserved.
//

import Foundation

protocol TicTacToeDelegate {
    func gameNeedRedrawBoard(game: TicTacToe)
    func gameDidOver(game: TicTacToe)
}

class TicTacToe {
    private let recursiveDepth = 2
    
    enum PlayerMove {
        case PlayerOneMove
        case PlayerTwoMove
    }
    
    enum GameStatus {
        case None
        case Playing
        case GameOver
    }
    
    enum GameWinner {
        case None
        case PlayerOneWin
        case PlayerTwoWin
        case Draw
    }
    
    enum GameScore: Int {
        case Zero = 0
        case Win = 10
        case Lose = -10
    }
    
    var boardObject: Board
    var beatAI: Bool = false
    var gameWinner: GameWinner = .None
    var computerFirstMove: Bool = false
    
    private var aiObject: AI
    private var moves: [Int] = []
    private var gameStatus: GameStatus = .None
    private var whichPlayerMove: PlayerMove = .PlayerOneMove
   
    var delegate: TicTacToeDelegate?
    
    init(initSquares: [Square]) {
        self.boardObject = Board(board:initSquares)
        self.aiObject = AI(board: self.boardObject)
    }
    
    func play(#beatAI: Bool, computerFirstMove: Bool) {
        if !beatAI && computerFirstMove {
            assertionFailure({ () -> String in
                return "computerFirstMove must be false if beatAI false."
            }())
        }
        
        self.beatAI = beatAI
        self.computerFirstMove = computerFirstMove
        gameStatus = .Playing
        
        if self.isComputerTurn() {
            nextMove()
        }
    }
    
    func changeSquareStatus(index: Int) {
        if boardObject.elementAtIndex(index) is SquareEmpty {
            switch whichPlayerMove {
            case .PlayerOneMove:
                boardObject.replaceElement(atIndex: index, withElement: SquareCross())
                whichPlayerMove = .PlayerTwoMove
            case .PlayerTwoMove:
                boardObject.replaceElement(atIndex: index, withElement: SquareCircle())
                whichPlayerMove = .PlayerOneMove
            }
            
            moves.append(index)
            checkBoard()
            delegate?.gameNeedRedrawBoard(self)
            
            if isComputerTurn() {
                nextMove()
            }
        }
    }
    
    func isComputerTurn() -> Bool {
        if beatAI && gameStatus == .Playing {
            if computerFirstMove {
                if whichPlayerMove == .PlayerOneMove {
                    return true
                }else {
                    return false
                }
            }else {
                if whichPlayerMove == .PlayerTwoMove {
                    return true
                }else {
                    return false
                }
            }
        }else {
            return false
        }
    }
    
    func nextMove() {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
            var nextMoveIndex: Int!
            
            if self.moves.count <= 2 {
                if self.boardObject.elementAtIndex(4) == SquareEmpty() {
                    nextMoveIndex = 4
                }else {
                    nextMoveIndex = [0, 2, 6, 8][Int(arc4random())%4]
                }
            }else {
                nextMoveIndex = self.aiObject.miniMax(self.recursiveDepth)
            }
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.changeSquareStatus(nextMoveIndex)
            })
        })

    }
    
    func resetGame() {
        whichPlayerMove = .PlayerOneMove
        gameWinner = .None
        gameStatus = .None
        moves.removeAll(keepCapacity: false)
        boardObject.cleanBoard()
        delegate?.gameNeedRedrawBoard(self)
    }
    
    func checkBoard() {
        let isWin = boardObject.isWin()
        
        if isWin {
            gameWinner = boardObject.winner()
            gameStatus = .GameOver
            delegate?.gameDidOver(self)
        }
        
        if !isWin && boardObject.isDraw() {
            gameWinner = .Draw
            gameStatus = .GameOver
            delegate?.gameDidOver(self)
        }
    }
}